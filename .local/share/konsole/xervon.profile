[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Solarized
Font=Source Code Pro,11.5,-1,5,50,0,0,0,0,0,Regular
UseFontLineChararacters=true

[General]
Name=xervon
Parent=FALLBACK/

[Interaction Options]
TrimTrailingSpacesInSelectedText=true

[Scrolling]
HistoryMode=1
HistorySize=5000
ScrollBarPosition=2
ScrollFullPage=false

[Terminal Features]
UrlHintsModifiers=67108864
