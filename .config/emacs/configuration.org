#+TITLE: Emacs Configuration
#+AUTHOR: Mika Dede <xervondev@gmail.com>

* Add Package Repositories
#+BEGIN_SRC emacs-lisp
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)
#+END_SRC

* Automatic package installation
** Install =use-package=
And its dependencies if needed.
#+BEGIN_SRC emacs-lisp
(mapc
 (lambda (package)
   (if (not (package-installed-p package))
       (progn
         (package-refresh-contents)
         (package-install package))))
 '(use-package diminish bind-key))
#+END_SRC
** Trigger =use-package=
And force the install of missing packages.
#+BEGIN_SRC emacs-lisp
(eval-when-compile
  (require 'use-package))
(require 'diminish)
(require 'bind-key)
#+END_SRC

* Set up evil
** Install =evil-mode=
#+BEGIN_SRC emacs-lisp
(use-package evil
  :ensure t ;; install the evil package if not installed
  :init ;; tweak evil's configuration before loading it
  (setq evil-search-module 'evil-search)
  (setq evil-ex-complete-emacs-commands nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-shift-round nil)
  (setq evil-want-C-u-scroll t)
  :config ;; tweak evil after loading it
  (evil-mode))
#+END_SRC
