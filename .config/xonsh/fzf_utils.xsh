def _pass(args, stdin=None):
    """Find all passwords that are not an otp code, copy selected one to clipboard"""
    path = $(rg --files @($HOME + '/.password-store/') -g '!otp.gpg' | sed 's/.*[.]password-store[/]//g;s/[.]gpg$//g' | fzf +m -q @(args[0] if args else '') | tr -d '\n')
    if path == '':
        return (None, 'Please choose a PASS entry', 1)
    $[pass -c @(path)]

aliases['p'] = _pass

def _otp(args, stdin=None):
    """Find all passwords that are otp codes, copy selected one to clipboard"""
    path = $(rg --files @($HOME + '/.password-store/') -g 'otp.gpg' | sed 's/.*[.]password-store[/]//g;s/[.]gpg$//g' | fzf +m -q @(args[0] if args else '') | tr -d '\n')
    if path == '':
        return (None, 'Please choose an OTP entry', 1)
    $[pass otp -c @(path)]

aliases['otp'] = _otp
