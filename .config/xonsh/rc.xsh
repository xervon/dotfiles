# XONSH WIZARD START
$AUTO_PUSHD = '1'
$COMPLETIONS_CONFIRM = '0'
$COMPLETIONS_DISPLAY = 'multi'
$DYNAMIC_CWD_ELISION_CHAR = '…'
$DYNAMIC_CWD_WIDTH = '20.0'
$HISTCONTROL = 'ignoredups'
$UPDATE_COMPLETIONS_ON_KEYPRESS = '1'
$XONSH_AUTOPAIR = '1'
$XONSH_COLOR_STYLE = 'solarizeddark'
$XONSH_HISTORY_MATCH_ANYWHERE = '1'
$XONSH_HISTORY_SIZE = '15778800.0 s'
$FOREIGN_ALIASES_SUPPRESS_SKIP_MESSAGE = True
xontrib load coreutils prompt_ret_code whole_word_jumping z powerline fzf-widgets vox vox_tabcomplete
# XONSH WIZARD END

source-bash $HOME/.asdf/asdf.sh

$SSH_AUTH_SOCK = '/tmp/ssh-xervon'
if ![ssh-add -l > /dev/null 2>&1].returncode == 1:
	ssh-add

$SXHKD_SHELL='/bin/bash'

import sys

sys.path.append($XDG_CONFIG_HOME + '/xonsh')

import fzf_utils

$EDITOR = 'edit'
$VISUAL = 'edit'

aliases['dots'] = 'git --work-tree=$HOME --git-dir=$HOME/.dotfiles'

aliases['l'] = 'ls -lhatp --color'
aliases['ll'] = 'ls -lAhtp --color'
aliases['e'] = 'edit -n'
aliases['v'] = 'edit -n'
